from telegram.ext import Application, CommandHandler, MessageHandler, filters, ContextTypes
from telegram import Bot, Update, MenuButtonCommands
from telegram import Update
from dotenv import load_dotenv
import openai
import os
import aiohttp
import json


# подгружаем переменные окружения
load_dotenv()

# передаем секретные данные в переменные
TOKEN = os.environ.get("TG_TOKEN")
GPT_SECRET_KEY = os.environ.get("GPT_SECRET_KEY")

# создаем бота
bot = Bot(token=TOKEN)

# передаем секретный токен chatgpt
openai.api_key = GPT_SECRET_KEY

# функция для асинхронного общения с chatgpt
async def get_answer_async(query, history):
    payload = {"text": query, "history": history}
    async with aiohttp.ClientSession() as session:
        async with session.post('http://127.0.0.1:5000/api/get_answer_async', json=payload) as resp:
            if resp.status == 200:
                response = await resp.json()
                return response
            else:
                return None, history

# функция-обработчик команды /start 
async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):

    # при первом запуске бота добавляем этого пользователя в словарь
    if update.message.from_user.id not in context.bot_data.keys():
        context.bot_data[update.message.from_user.id] = 3
    
    # возвращаем текстовое сообщение пользователю
    await update.message.reply_text('Задайте любой вопрос консультанту')


# функция-обработчик команды /help 
async def help(update: Update, context: ContextTypes.DEFAULT_TYPE):
    
    # Список команд с описаниями
    commands = {
        "start": "Начать беседу",
        "status": "проверка доступного количества запросов",
        "history": "сохранение истории контекста",
        "data": "сохранение информации о пользователе",
        "help": "справка по командам"
    }
    # Собираем список команд в одно сообщение, добавляя спецсимвол и описание
    help_text = "\n".join([f"/{command} - {description}" for command, description in commands.items()])
    
    await update.message.reply_text(help_text)


# функция-обработчик команды /data 
async def data(update: Update, context: ContextTypes.DEFAULT_TYPE):

    # создаем json и сохраняем в него словарь context.bot_data
    with open('data.json', 'w') as fp:
        json.dump(context.bot_data, fp)
        
    # возвращаем текстовое сообщение пользователю
    await update.message.reply_text('Данные пользователя записаны в файл "data.json"')


# функция-обработчик команды /history 
async def history(update: Update, context: ContextTypes.DEFAULT_TYPE):
    # получаем историю вопросов и ответов из контекста
    # и сохраняем историю в файл
    with open('history.json', 'w') as file:
        json.dump(context.chat_data.get('qa_history', []), file, ensure_ascii=False, indent=4)
    
    with open('history.json', 'r', encoding='utf8') as fp:
        history = fp.read()
        
    # Отправляем сообщение пользователю
    await update.message.reply_text('История сохранена в файл "history.json". \n\nВот его содержимое: \n' + history)
 
 
# функция-обработчик текстовых сообщений
async def text(update: Update, context: ContextTypes.DEFAULT_TYPE):
    
    # устанавливаем MenuButton
    menu_button = MenuButtonCommands()
    await bot.set_chat_menu_button(menu_button=menu_button)

    user_id = update.message.from_user.id  # Получаем ID пользователя для использования в контексте
    
    # проверка доступных запросов пользователя
    if context.bot_data.get(user_id, 0) > 0:
        # выполнение запроса в chatgpt
        first_message = await update.message.reply_text('Ваш запрос обрабатывается, пожалуйста подождите...')
        
        # получаем текст запроса
        query = update.message.text

        # получаем историю вопросов и ответов из контекста
        history = context.chat_data.get('qa_history', [])
        
        # получаем ответ с помощью асинхронной функции
        res = await get_answer_async(query, history)
        await context.bot.edit_message_text(text=res['message'], chat_id=update.message.chat_id, message_id=first_message.message_id)
        
        # Создаем словарь с вопросом и ответом
        history_pair = {
            "Question": query,
            "Answer": res['message']
        }

        # Добавляем словарь в список истории вопросов и ответов
        history.append(history_pair)
        
        # обрезаем список истории вопросов и ответов до 5 элементов
        context.chat_data['qa_history'] = history[-5:]
        
        # уменьшаем количество доступных запросов на 1
        context.bot_data[user_id] -= 1
             
    else:
        # сообщение если запросы исчерпаны
        await update.message.reply_text('Ваши запросы на сегодня исчерпаны')
    
        
# Функция-обработчик команды /status
async def status(update: Update, context: ContextTypes.DEFAULT_TYPE):
    
    # проверка доступных запросов пользователя
    if context.bot_data[update.message.from_user.id] > 0:
        
        # возвращаем количество доступных запросов пользователю
        await update.message.reply_text(f'Осталось запросов: {context.bot_data[update.message.from_user.id]}')
        
    else:
        
        # сообщение если запросы исчерпаны
        await update.message.reply_text('Ваши запросы на сегодня исчерпаны')


# функция, которая будет запускаться раз в сутки для обновления доступных запросов
async def callback_daily(context: ContextTypes.DEFAULT_TYPE):

    # проверка базы пользователей
    if context.bot_data != {}:

        # проходим по всем пользователям в базе и обновляем их доступные запросы
        for key in context.bot_data:
            context.bot_data[key] = 15
        print('Запросы пользователей обновлены')
    else:
        print('Не найдено ни одного пользователя')


def main():

    # создаем приложение и передаем в него токен бота
    application = Application.builder().token(TOKEN).build()
    print('Бот запущен...')

    # создаем job_queue 
    job_queue = application.job_queue
    job_queue.run_repeating(callback_daily, # функция обновления базы запросов пользователей
                            interval=600,    # интервал запуска функции (в секундах)
                            first=10)       # первый запуск функции (через сколько секунд)

    # добавление обработчиков
    application.add_handler(CommandHandler("start", start, block=False))
    application.add_handler(CommandHandler("data", data, block=False))
    application.add_handler(CommandHandler("status", status, block=False))
    application.add_handler(CommandHandler("history", history, block=False))
    application.add_handler(CommandHandler("help", help, block=False))
    application.add_handler(MessageHandler(filters.TEXT, text, block=False))
    
    # запуск бота (нажать Ctrl+C для остановки)
    application.run_polling()
    print('Бот остановлен')


if __name__ == "__main__":
    main()