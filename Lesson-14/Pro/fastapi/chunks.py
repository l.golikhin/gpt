from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores import FAISS
from langchain.docstore.document import Document
from dotenv import load_dotenv
import openai
import os


# получим переменные окружения из .env
load_dotenv()

# API-key
openai.api_key = os.environ.get("OPENAI_API_KEY")

# задаем system
default_system = "Ты-консультант в компании Simble, ответь на вопрос клиента на основе документа с информацией. Не придумывай ничего от себя, отвечай максимально по документу. Не упоминай Документ с информацией для ответа клиенту. Клиент ничего не должен знать про Документ с информацией для ответа клиенту"

with open('prompt.txt', 'r') as file:
    prompt = file.read()

class Chunk():

    def __init__(self, path_to_base:str, sep:str=" ", ch_size:int=1024):
        
        # загружаем базу
        with open(path_to_base, 'r', encoding='utf-8') as file:
            document = file.read()

        # создаем список чанков
        source_chunks = []
        splitter = CharacterTextSplitter(separator=sep, chunk_size=ch_size)
        for chunk in splitter.split_text(document):
            source_chunks.append(Document(page_content=chunk, metadata={}))

        # создаем индексную базу
        embeddings = OpenAIEmbeddings()
        self.db = FAISS.from_documents(source_chunks, embeddings)
        
        
    # функция асинхронного получения ответа
    async def async_get_answer(self, system:str = prompt, query:str = None, history_context:list = None):
        '''
            Асинхронная функция получения ответа от chatgpt
        '''
        # релевантные отрезки из базы
        docs = self.db.similarity_search(query, k=4)
        message_content = '\n'.join([f'{doc.page_content}' for doc in docs])
        
        # формируем историю контекста для модели
        messages = []

        # добавляем текущий вопрос и информацию из документа
        messages.append({"role": "system", "content": system})
        messages.append({"role": "user", "content": f"Ответь на вопрос клиента. Не упоминай документ с информацией для ответа клиенту в ответе. Документ с информацией: {message_content}\n\nВопрос клиента: \n{query}"})
        # messages.append({"role": "assistant", "content": message_content})
        
        if history_context:
            history = '\n'.join(f"Вопрос: {context['Question']}, \nОтвет: {context['Answer']}\n\n" for context in history_context)
            messages.append({"role": "user", "content": f"История контекста (вопросы и ответы): \n{history}"})
               
        # получение ответа от chatgpt
        completion = await openai.ChatCompletion.acreate(model="gpt-3.5-turbo-1106",
                                                  messages=messages,
                                                  temperature=0)
        
        # возвращаем ответ, обновлённую историю и список сообщений  в виде словаря
        return {
            "answer": completion.choices[0].message.content,
            "history": history_context,
            "messages": messages
        }
