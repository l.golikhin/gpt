from fastapi import FastAPI
from chunks import Chunk
import json
from pydantic import BaseModel
from concurrent.futures import ThreadPoolExecutor
from fastapi.middleware.cors import CORSMiddleware

# инициализация индексной базы
chunk = Chunk(path_to_base="Simble.txt")

# класс с типами данных параметров 
class Item(BaseModel): 
    text: str
    history: list

# создаем объект приложения
app = FastAPI()

# объявляем переменную для хранения истории контекста
history = []

# настройки для работы запросов
app.add_middleware(
    CORSMiddleware,
    allow_origins="*",
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# функция обработки get запроса + декоратор 
@app.get("/")
def read_root():
    return {"message": "answer"}


# асинхронная функция обработки post запроса + декоратор 
@app.post("/api/get_answer_async")
async def get_answer_async(question: Item):
    print(f'Text: {question.text}', '\n\n')
    print(f'History: {question.history}', '\n\n')
    answer = await chunk.async_get_answer(
                query=question.text, 
                history_context=question.history
            )
    
    print(answer['answer'], '\n\n')
    print(answer['history'], '\n\n')
    print(answer['messages'], '\n\n')
    global history
    history = answer['history']
    
    return {"message": answer['answer']}

# функция получения истории контекста
@app.get("/api/get_history")
async def get_history():
    return history

