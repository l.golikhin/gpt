from telegram.ext import Application, CommandHandler, CallbackQueryHandler, MessageHandler, ContextTypes, filters
from telegram import InlineKeyboardMarkup, Update, InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup
from dotenv import load_dotenv
import os

# подгружаем переменные окружения
load_dotenv()

# токен бота
TOKEN = os.getenv('TG_TOKEN')

# Создаем клавиатуру с выбором языка
language_keyboard = InlineKeyboardMarkup([
    [InlineKeyboardButton("Русский", callback_data="RUS")],
    [InlineKeyboardButton("English", callback_data="ENG")]
])

# Глобальная переменная для хранения выбранного языка
selected_language = None

# ===================================
# Блок функций-обработчиков команд
# ===================================   

# функция- обработчик команды /start
async def start (update: Update, _):
    # прикрепляем inline клавиатуру к сообщению
    await update.message.reply_text("Выберите язык интерфейса:", reply_markup=language_keyboard)

# функция- обработчик команды /help
async def help (update: Update, _):
    # прикрепляем inline клавиатуру к сообщению
    await update.message.reply_text("Справка")    
    
# Функция-обработчик текстового сообщения
async def text (update: Update, _):
    
    if selected_language == "RUS":
        await update.message.reply_text("Текстовое сообщение получено!")
    elif selected_language == "ENG":
        await update.message.reply_text("We’ve received a message from you!")
        
# Функция-обработчик голосового сообщения
async def voice (update: Update, context: ContextTypes.DEFAULT_TYPE):
    
    # в ответ на голосовое сообщение прикрепляем картинку и подпись на выбранном языке
    if selected_language == "RUS":
        await update.message.reply_photo("images/photo_voices.jpg", caption="Голосовое сообщение получено!")
    elif selected_language == "ENG":
        await update.message.reply_photo("images/photo_voices.jpg", caption="We’ve received a voice message from you!")
        
    
# Функция-обработчик фотографии
async def photo (update: Update, _):
    # получаем файл
    file = await update.message.photo[-1].get_file()
     
    # сохраняем файл на диск
    await file.download_to_drive(f"photos/Image_{update.message.message_id}.jpg")
     
    # Вывод сообщения после успешного сохранения
    if selected_language == "RUS":
        await update.message.reply_text("Фотография сохранена!")
    elif selected_language == "ENG":
        await update.message.reply_text("Photo saved!")
        
 
# Функция-обработчик выбора языка
async def button (update: Update, _):
    global selected_language
    
    # получаем callback query из update
    query = update.callback_query
    
    # В зависимости от выбора языка выводим сообщение на соответствующем языке
    if query.data == "RUS":
        selected_language = query.data
        await query.edit_message_text(text=f"Выбран язык: {selected_language}")
    elif query.data == "ENG":
        selected_language = query.data
        await query.edit_message_text(text=f"Selected language: {selected_language}")

    # Вывод уведомления о выбранном языке
    await query.answer(f'Переключено на: [ {query.data} ]')
 

# ===================================
# Блок функций для создания и запуска ТГ-бота
# ===================================    

# Функция для создания приложения
def create_application():
    # Создаем экземпляр приложения с переданным токеном
    application = Application.builder().token(TOKEN).build()
    message = print('Бот запущен...')
    # Возвращаем созданный экземпляр приложения
    return application, message

# Функция для регистрации обработчиков
def register_handlers(application):
    application.add_handler(CommandHandler("start", start))  # Добавляем обработчик команды "/start"
    application.add_handler(MessageHandler(filters.TEXT, text))  # Добавляем обработчик текстовых сообщений
    application.add_handler(MessageHandler(filters.VOICE, voice))  # Добавляем обработчик голосовых сообщений
    application.add_handler(MessageHandler(filters.PHOTO, photo))  # Добавляем обработчик фотографий
    application.add_handler(CallbackQueryHandler(button))  # Добавляем обработчик нажатия кнопки

# Функция для запуска приложения
def run_application(application):
    application.run_polling()  # Запускаем приложение в режиме опроса
    print('\nБот остановлен')  # Выводим сообщение остановки бота

# Главная функция, которая вызывает функции создания бота и регистрации обработчиков
def main():
    application = create_application()[0]
    register_handlers(application)  # Регистрируем обработчики
    run_application(application)  # Запускаем приложение

# Вызываем главную функцию
if __name__ == '__main__':
    main()