from django.shortcuts import render

# render start.html page
def start(request):
    return render(request, "start.html")

# render chatbot.html page
def chatbot(request):
    return render(request, "chatbot.html")

# render stats.html page
def stats(request):
    # Создаем список часов с форматированием "00" до "23"
    hours = [f'{hour:02d}' for hour in range(24)]
    # Передаем этот список в контекст шаблона
    return render(request, 'stats.html', {'hours': hours})
