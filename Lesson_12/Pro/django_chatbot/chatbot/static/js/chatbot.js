const messagesList = document.querySelector('.messages-list');
const messageForm = document.querySelector('.message-form');
const messageInput = document.querySelector('.message-input');

messageForm.addEventListener('submit', (event) => {

    event.preventDefault();
    console.log("asd");
    const message = messageInput.value.trim();
    if (message.length === 0) {
        return;
    }

    const messageItem = document.createElement('li');
    messageItem.classList.add('message', 'sent');
    messageItem.innerHTML = `
        <div class="message-text">
          <div class="message-sender">
            <b>Вы</b>
          </div>
          <div class="message-content">
            ${message}
          </div>
        </div>`;
    messagesList.appendChild(messageItem);

    // Очистка поля ввода и отправка сообщения консультанту
    messageInput.value = '';

    fetch('http://127.0.0.1:5000/api/get_answer', {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ "text": message })
    })
        .then(response => response.json())
        .then(data => {
            const response = data.message;
            const messageItem = document.createElement('li');
            messageItem.classList.add('message', 'received');
            messageItem.innerHTML = `
        <div class="message-text">
            <div class="message-sender">
              <b>AI Консультант</b>
            </div>
            <div class="message-content">
                ${response}
            </div>
        </div>`;
            messagesList.appendChild(messageItem);
        });
});
