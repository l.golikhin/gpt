// Функция для обновления статистики в таблице
function updateStatsTable() {
    const url = 'http://127.0.0.1:5000/stats';

    // Очистка таблицы
    fetch(url)
        //
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok ' + response.statusText);
            }
            return response.json();
        })
        // Обработка успешного ответа
        .then(data => {
            // console.log(data); // Временный вывод данных в консоль для отладки
            data.forEach(item => {
                // Обновление количества запросов в соответствующем элементе
                const requestsElement = document.getElementById(`requests-hour-${item.Hour}`);
                if (requestsElement) {
                    const totalRequests = item.Requests.reduce((sum, current) => sum + current.Count, 0);
                    requestsElement.textContent = totalRequests;
                }
                // Обновление списка эндпоинтов в соответствующем элементе с переносами строк
                const endpointsElement = document.getElementById(`endpoints-hour-${item.Hour}`);
                if (endpointsElement) {
                    const endpointsList = item.Requests.map(endpointInfo => `${endpointInfo.Endpoints} (${endpointInfo.Count})`).join('<br>');
                    endpointsElement.innerHTML = endpointsList;
                }
            });
            // После обновления таблицы вызываем функцию создания графика
            createStatsChart(data);
        })
        // Обработка ошибок
        .catch(error => {
            console.error('Error fetching stats:', error);
        });
}

// Обновление таблицы и графика
updateStatsTable();
setInterval(updateStatsTable, 60000);

// Добавление графика
// Глобальная переменная для хранения экземпляра графика
let myChart = null;

// Функция для создания графика
function createStatsChart(data) {
    const ctx = document.getElementById('statsChart').getContext('2d');

    // Если график уже создан, обновим его данные
    if (myChart) {
        myChart.data.labels = data.map(item => `${item.Hour}:00`);
        myChart.data.datasets[0].data = data.map(item => item.Requests.reduce((sum, current) => sum + current.Count, 0));
        myChart.update();
    } else {
        // Создание нового графика
        myChart = new Chart(ctx, {
            type: 'bar', // или 'line', 'pie', в зависимости от типа графика
            data: {
                labels: data.map(item => `${item.Hour}:00`), // Метки по оси X
                datasets: [{
                    label: 'Количество запросов', // Подпись данных
                    data: data.map(item => item.Requests.reduce((sum, current) => sum + current.Count, 0)), // Данные для графика
                    backgroundColor: 'rgba(0, 123, 255, 0.5)', // Цвет фона для столбцов
                    borderColor: 'rgba(0, 123, 255, 1)', // Цвет границы столбцов
                    borderWidth: 1
                }]
            },
            // Настройки графика
            options: {
                scales: {
                    y: {
                        beginAtZero: true // Начинать ось Y с нуля
                    }
                },
                // Настройки легенды
                plugins: {
                    legend: {
                        display: false // Скрыть легенду
                    }
                }
            }
        });
    }
}