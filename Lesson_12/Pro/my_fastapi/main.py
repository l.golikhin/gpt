# import schedule                       # для создания задач
# import time                           # для ожидания выполнения
# import threading                      # для создания потоков
from datetime import datetime
from fastapi import FastAPI, Request
from pydantic import BaseModel
from chunks import Chunk
from starlette.responses import JSONResponse
from collections import defaultdict
from concurrent.futures import ThreadPoolExecutor
from fastapi.middleware.cors import CORSMiddleware

# Инициализация объекта Chunk (используем вашу реализацию)
chunk = Chunk(path_to_base="Docs_from_lesson_6.txt")

# Открытие файла "Prompt.txt" в режиме чтения
with open("Prompt.txt", 'r', encoding='utf-8') as f:
    prompt = f.read()

# Класс с типами данных параметров 
class Item(BaseModel): 
    text: str

# Создание экземпляра приложения FastAPI
app = FastAPI()

# Инициализация словаря для подсчета запросов к эндпоинтам за текущий час
endpoint_request_count = defaultdict(lambda: defaultdict(int))

# Пул соединений для API OpenAI
openai_executor = ThreadPoolExecutor(max_workers=10)

# Миддлвейр для подсчета запросов
@app.middleware("http")
async def count_requests(request, call_next):
    response = await call_next(request)
    current_hour = datetime.now().strftime("%H")
    endpoint_request_count[current_hour][request.url.path] += 1
    return response

# Конфигурация CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins="*",
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

# Endpoint для получения ответа от AI-консультанта
@app.post("/api/get_answer")
def get_answer(question: Item):
    answer = chunk.get_answer(query=question.text, system=prompt)
    return {"message": answer}

# Endpoint для получения статистики
@app.get("/stats")
async def get_all_stats():
    stats_data = []
    for hour in range(24):
        hour_str = str(hour).zfill(2)
        # Если за данный час были запросы, формируем список, иначе возвращаем пустой список
        requests_data = [
            {"Endpoints": endpoint, "Count": count}
            for endpoint, count in endpoint_request_count[hour_str].items() if count > 0
        ] if endpoint_request_count[hour_str] else []

        stats_data.append({"Hour": hour_str, "Requests": requests_data})
    return JSONResponse(stats_data)

# Endpoint для получения общего количества запросов
@app.get("/stats/requests")
async def get_total_request_count():
    current_hour = datetime.now().strftime("%H")
    total_count = sum(count for endpoint, count in endpoint_request_count[current_hour].items())
    response = {
        "Общее количество запросов пользователей": total_count,
    }
    return JSONResponse(response)

# ===========================
# Закомментированный блок ниже для создания планировщика и сброса статистики
# ===========================

# # Функция, которая будет выполнять планировщик
# def run_scheduler():
#     while True:
#         schedule.run_pending()
#         time.sleep(1)

# # Блок для сброса статистики
# def reset_stats():
#     # Очистка словаря с запросами
#     endpoint_request_count.clear()

# # Запуск планировщика в отдельном потоке
# scheduler_thread = threading.Thread(target=run_scheduler)
# scheduler_thread.start()

# # Запуск планировщика для сброса статистики
# schedule.every().day.at("00:00").do(reset_stats)

# # Запуск планировщика
# while True:
#     schedule.run_pending()
#     time.sleep(1)
    
# # Запуск сервера FastAPI
# if __name__ == "__main__":
#     import uvicorn
#     uvicorn.run(app, host="127.0.0.1", port=5000)