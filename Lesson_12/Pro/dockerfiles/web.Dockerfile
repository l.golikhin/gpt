FROM python:3.11.6-slim

WORKDIR /app

COPY ./django_chatbot $WORKDIR

RUN pip install --upgrade pip && pip install -r requirements.txt

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "172.16.20.11:8000"]