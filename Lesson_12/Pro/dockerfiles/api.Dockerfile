FROM python:3.11.6-slim

WORKDIR /app

COPY ./my_fastapi $WORKDIR

RUN pip install --upgrade pip && pip install -r requirements.txt

EXPOSE 5000

CMD ["uvicorn", "main:app", "--host", "172.16.20.12", "--port", "5000"]
